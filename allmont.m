clear all;
clc;
close all;
Ms = [];
Ts = [];
Ns = [];
Es = [];
sigma = [];
maxsig = [];
N = 100;
h = 100;
r = rand(1,h);
Ta = r.*5+1e-10;
%% Monte Carlo Loop

for n=1:h;
    % Choose a temperature
    T=Ta(n);
    % Perform a simulation
    J = 1; % Strength of interaction (Joules)
    k = 1; % Joules per kelvin
    grid = (rand(N) > 0.5)*2 - 1;
    for i=1:500,
        % Calculate the number of neighbors of each cell
        neighbors = circshift(grid, [ 0 1]) + ...
        circshift(grid, [ 0 -1]) + ...
        circshift(grid, [ 1 0]) + ...
        circshift(grid, [-1 0]);
        % Calculate the change in energy of flipping a spin
        DeltaE = 2 * J * (grid .* neighbors);
        % Calculate the transition probabilities
        p_trans = exp(-DeltaE/(k * T));
        % Decide which transitions will occur
        transitions = (rand(N) < p_trans ).*(rand(N) < 0.1) * -2 + 1;
        % Perform the transitions
        grid = grid .* transitions;
        % Sum up our variables of interest
        % M = sum(sum(grid));
        % E = -sum(sum(DeltaE))/2;
        % Display the current state of the system (optional)
        % image((grid+1)*128);
        % xlabel(sprintf('T = %0.2f, M = %0.2f, E = %0.2f', T, M/N^2, E/N^2));
        % set(gca,'YTickLabel',[],'XTickLabel',[]);
        % axis square; colormap bone; drawnow;
    end
    Y=fft2(grid);
    s=(Y.*conj(Y))/N;

    %figure(4)
    %plot(s) ;
    %xlim([0 N/2]);
    sigma=[sigma s];
    maxsig=[maxsig max(max(sigma))];
    M = sum(sum(grid));
    E = -sum(sum(DeltaE))/2;
    %figure(1)
    %image((grid+1)*128);
    %xlabel(sprintf('T = %0.2f, M = %0.2f, E = %0.2f', T, M/N^2, E/N^2));
    %set(gca,'YTickLabel',[],'XTickLabel',[]);
    %axis square; colormap bone; drawnow;

    % Count the number of clusters of �spin up� states
    [L, num] = bwlabel(grid == 1, 4);
    % Record the results
    Ms = [Ms M/(N^2)];
    Es = [Es E/(N^2)];
    Ns = [Ns N];
    Ts = [Ts T];
end

%% Figure Generation
% Energy per site, versus temperature

figure(1)
plot(Ts, Es, 'ro');
ylabel('energy per site');
xlabel('temperature');
pbaspect([2 1 1]);
print(gcf, '-depsc2', 'ising-energy');
figure(2)
% Magnetization per site, versus temperature
plot(Ts, Ms, 'bo');
ylabel('magnetization per site');
xlabel('temperature');
ylim([-1.1 1.1]);
pbaspect([2 1 1]);
print(gcf, '-depsc2', 'ising-magnetization');
% Magnetization per site, versus Energy per site
figure(3)
plot(Es, Ms, 'o', 'Color', [0 0.5 0]);
xlabel('Energy per site');
ylabel('Magnetization per site');
pbaspect([2 1 1]);
print(gcf, '-depsc2', 'ising-mvse')
figure(4)
plot(sigma);
xlim([0 n_grid/2]);
figure(5)
plot(Ta,maxsig);

