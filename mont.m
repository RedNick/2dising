clear all;
clc;
close all;
% Defines empty variables for later usage
Ss = cell(1);

% n_grid = input('Please select the grid size \n');
% h = input('Please select the number of temperature ranges to use \n');
n_grid = 500; % Sets grid size (root(number of particles))
h=20; % Number of different temperatures the simulation is done over.
pr='n';
% pr = input('Do you want any lattice plots? y/n \n', 's');
r=rand(1,h); % Sets the range of temperatures to between e-10 and ~5.
Ta = r.*5+1e-10;
Ta=sort(Ta);

%% Monte Carlo Loop
tic
for n=1:h;
	T=Ta(n); % Picks a the nth temperature
	% Performs a simulation
	s=0;
	for count=1:50
		[sn] = ising(n_grid, T, pr);
		s=s+sn;
	end
	s=s/count;
	
	% Records the results
	Ss{n}=s;
end
toc
%%
[xGrid, yGrid]=meshgrid((1:n_grid));
rGrid=sqrt((xGrid-(n_grid+1)/2).^2+(yGrid-(n_grid+1)/2).^2);
rLine=reshape(rGrid,1,n_grid^2);

L = [];
for n=1:h;
    L=[L ;sprintf('T=%0.2f',Ta(n))];
end;

figure(6)
hold all
for n=1:h
	Ss2=reshape(Ss{n},1,n_grid^2);
	Ss2=[rLine;Ss2];
	Ss2=sortrows(Ss2');
    o=1;    
    k=Ss2(1,2);
    p=1;
    while (p+o)<=n_grid^2;
        k=Ss2(p+o-1,2);
        while (p+o)<=n_grid^2    &&  Ss2(p,1)== Ss2(p+o,1)       
            k=Ss2(p+o,2)+k;
            o=o+1;            
        end;        
        Ss2(p:p+o-1,2)=k/o;        
        p=p+o;        
        o=1;
    end
	plot(Ss2(:,1),Ss2(:,2))
% 	plot(Ss{n}(50,:));
% 	imagesc(Ss{n});
end

title('Plot of ffts');
xlabel('');
ylabel('');
legend(L);


