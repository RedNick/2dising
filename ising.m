function [s] = ising(N,T,pr)
J = 1; % Strength of interaction (Joules)
k = 1; % Joules per kelvin

%% Generate a random initial configuration
grid = (rand(N) > 0.5)*2 - 1;
%% Evolve the system for a fixed number of steps
for i=1:200,
%     Calculate the number of neighbors of each cell
    neighbors = circshift(grid, [ 0 1]) + ...
    circshift(grid, [ 0 -1]) + ...
    circshift(grid, [ 1 0]) + ...
    circshift(grid, [-1 0]);
%   Calculate the change in energy of flipping a spin
    DeltaE = 2 * J * (grid .* neighbors);
%   Calculate the transition probabilities
    p_trans = exp(-DeltaE/(k * T));
%   Decide which transitions will occur
    transitions = (rand(N) < p_trans ).*(rand(N) < 0.1) * -2 + 1;
%   Perform the transitions
    grid = grid .* transitions;
end
Y=fft2(grid);
Y=fftshift(Y);

s=(Y.*conj(Y))/N;

if pr == 'y';
    figure(1)
    image((grid+1)*128);
    xlabel(sprintf('T = %0.2f, M = %0.2f, E = %0.2f', T, M/N^2, E/N^2));
    set(gca,'YTickLabel',[],'XTickLabel',[]);
    axis square; colormap gray; drawnow;
    p=input('Print this image? y/n \n', 's');
    
    if p == 'y' 
        print(gcf, '-dpdf', sprintf('lattice T=%0.2f.pdf',T));
    end
    
    figure(2)
    plot(s(1:N/2));
    p2=input('Print this image? y/n \n', 's');
    if p2 == 'y'
        print(gcf, '-dpdf', sprintf('fourier T=%0.2f.pdf',T));
    end
end
% Count the number of clusters of spin up and spin down states
[L, num] = bwlabel(grid == 1, 4);