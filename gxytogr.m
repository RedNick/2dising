%This code takes in the saved g variable, the spin-spin correlation, and
%outputs a plot of g(x,y), and g(r).
%This just opens the file storing our correlation data from a previous
%programme.
clear all

load('rb70data','g')

as = find(g ~= 0);
rb = as(1)-2;
L = 5*rb;




%This is the array that will represent g(r)
sizeg = size(g)./2;
rmax = sqrt(sizeg(1).^2 + sizeg(2).^2);
rmax = round(rmax);
gr = zeros(1,rmax);
for r = 1:rmax
    %This sets up a grid of points over all the lattice.
    y = 1:L;     %Set to L.
    x = 1:L;
    [X Y] = meshgrid(x,y);
    %This creates the radius at which we want to find g(r), and draws a
    %circle there.
    R2 = (X).^2+(Y).^2;
    c = contourc(x,y,R2,[0 0]+r^2);
    %This line finds the parts of the g(x,y) that are ~ on the circle.
    c = round(c(:,2:end));
    %This takes the values of g(x,y)
    Ac = g(sub2ind(size(g),c(2,:),c(1,:))); 
    %This averages them to get g(r).
    val = mean(Ac);
    gr(r) = val;
end

figure(1),h = pcolor(-g);,colormap gray,axis equal
axis([1 L 1 L]),set(h,'EdgeColor','none')
title(['g^{(2)}(x,y) for a 2D Lattice with L = 10R_b = ',num2str(2*L)]);
xlabel('X Displacement')
ylabel('Y Displacement')
sizegr = size(gr);
normalfactor = sum(gr((3*sizegr(2)/4):sizegr(2)))./(sizegr(2)./4);
gr = gr./normalfactor;
x = (1:sizegr(2))./rb;
figure(2),plot(x,gr)
title(['g^{(2)}(r) for a 2D Lattice with L = 10R_b = ',num2str(2*L)])
xlabel('r/R_b')
ylabel('g^{(2)}(r)')
xlim([0 x(end)])